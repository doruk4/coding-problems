# Two strings are considered close if you can attain one from the other using the following operations:

#     Operation 1: Swap any two existing characters.
#         For example, abcde -> aecdb
#     Operation 2: Transform every occurrence of one existing character into another existing character, and do the same with the other character.
#         For example, aacabb -> bbcbaa (all a's turn into b's, and all b's turn into a's)

# You can use the operations on either string as many times as necessary.

# Given two strings, word1 and word2, return true if word1 and word2 are close, and false otherwise.

 def closeStrings(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: bool
        """

        if len(word1) != len(word2):
            return False

        operation_one = False
        count = 0

        for letter in word2:
            if letter in word1:
                count += 1

        if count == len(word1):
            operation_one = True

        operation_two = False

        map_one = {}
        map_two = {}

        for letter in word1:
            if letter not in map_one:
                map_one[letter] = 1
            else:
                map_one[letter] += 1

        for letter in word2:
            if letter not in map_two:
                map_two[letter] = 1
            else:
                map_two[letter] += 1

        operation_two = sorted(map_one.values()) == sorted(map_two.values())

        if operation_one == True and operation_two == True:
            return True
