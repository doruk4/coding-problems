# You are given an integer array matches where matches[i] = [winneri, loseri] indicates that the player winneri defeated player loseri in a match.

# Return a list answer of size 2 where:

#     answer[0] is a list of all players that have not lost any matches.
#     answer[1] is a list of all players that have lost exactly one match.

# The values in the two lists should be returned in increasing order.

def findWinners(self, matches):
        """
        :type matches: List[List[int]]
        :rtype: List[List[int]]
        """

        undefeated = []
        one = []
        player_map = {}

        for match in matches:
            if match[0] not in player_map:
                player_map[match[0]] = 0
            if match[1] not in player_map:
                player_map[match[1]] = 1
            elif match[1] in player_map:
                player_map[match[1]] += 1

        for player in player_map:
            if player_map[player] == 0:
                undefeated.append(player)
            elif player_map[player] == 1:
                one.append(player)

        return [sorted(undefeated),sorted(one)]
