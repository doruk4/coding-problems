# You are given a string s of even length. Split this string into two halves of equal lengths, and let a be the first half and b be the second half.

# Two strings are alike if they have the same number of vowels ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'). Notice that s contains uppercase and lowercase letters.

# Return true if a and b are alike. Otherwise, return false.


def halvesAreAlike(self, s):
        """
        :type s: str
        :rtype: bool
        """

        vowels = "aeiou"

        first, second = s[:len(s)//2], s[len(s)//2:]
        first_count = 0
        second_count = 0

        for letter in first:
            if letter.lower() in vowels:
                first_count += 1

        for letter in second:
            if letter.lower() in vowels:
                second_count += 1

        return first_count == second_count
