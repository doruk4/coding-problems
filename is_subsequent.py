# Given two strings s and t, return true if s is a subsequence of t, or false otherwise.

def isSubsequence(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """

        i = 0

        for letter in t:
            if i < len(s) and letter == s[i]:
                i += 1

        return i == len(s)
