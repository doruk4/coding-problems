# You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

# Find two lines that together with the x-axis form a container, such that the container contains the most water.

# Return the maximum amount of water a container can store.

def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """


        max = 0
        i = 0
        j = len(height)-1

        while i < j:
            if (min(height[j], height[i]) * (j-i)) > max:
                max =  min(height[j], height[i]) * (j-i)

            if height[i] < height[j]:
                i += 1

            else:
                j -= 1


        return max
