# You are given two strings word1 and word2. Merge the strings by adding letters in alternating order, starting with word1.
# If a string is longer than the other, append the additional letters onto the end of the merged string.
# Return the merged string.

def mergeAlternately(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: str
        """
        merged = ""

        if len(word1) == len(word2):
            for i,j in zip(word1, word2):
                merged += i
                merged += j

        elif len(word1) > len(word2):
            n = len(word2)
            sliced_word = word1[:n]
            for i,j in zip(sliced_word,word2):
                merged+= i
                merged+= j
            merged+= word1[n:]

        elif len(word1) < len(word2):
            n = len(word1)
            sliced_word = word2[:n]
            for i,j in zip(word1,sliced_word):
                merged+= i
                merged+= j
            merged+= word2[n:]

        return merged
