# You are given two strings of the same length s and t. In one step you can choose any character of t and replace it with another character.

# Return the minimum number of steps to make t an anagram of s.

# An Anagram of a string is a string that contains the same characters with a different (or the same) ordering.

def minSteps(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: int
        """

        res = 0
        s_set = set(s)

        for char in s_set:
            diff = s.count(char) - t.count(char)
            if diff > 0:
                res += diff
        return res
