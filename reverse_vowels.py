# Given a string s, reverse only all the vowels in the string and return it.

# The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """

        vowels = ""

        for letter in s:
            if letter.lower() in "aeiou":
                vowels += letter

        reversed_vowels = vowels[::-1]
        result = ""

        for letter in s:
            if letter.lower() in "aeiou":
                result += reversed_vowels[0]
                reversed_vowels = reversed_vowels[1:]
            else:
                result += letter

        return result
