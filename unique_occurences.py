# Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.

def uniqueOccurrences(self, arr):
        """
        :type arr: List[int]
        :rtype: bool
        """

        values = {}
        result = []
        for num in arr:
            if num not in values:
                values[num] = 1
            if num in values:
                values[num] += 1

        for k,v in values.items():
            result.append(v)

        for val in result:
            if result.count(val) > 1:
                return False
        return True
